import {Redirect, Route} from "react-router-dom";
import React from "react";
import ProductList from "../components/product-list/ProductList";
import ProductInput from "../components/add-product/ProductInput";
import Switch from "react-router-dom/es/Switch";
import Router from "react-router-dom/es/Router";
import {createBrowserHistory} from "history";

export const Routes = () => {
    return (
        <Router history={createBrowserHistory()}>
            <Switch>
                <Route exact path="/">
                    <Redirect to="/productList" />
                </Route>
                <Route path="/productList">
                    <div className="SplitPane">
                        <div className="SplitPane-left">
                            <ProductInput/>
                        </div>
                        <div className="SplitPane-right">
                            <ProductList/>
                        </div>
                    </div>
                </Route>
            </Switch>
        </Router>
    );
};