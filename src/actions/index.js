export function changeQuantity(id, newQuantity) {
    return {
        type: "QUANTITY_CHANGE",
        id: id,
        quantity: newQuantity
    }
}

export function deleteProductEntry(id) {
    return {
        type: "DELETE_FROM_LIST",
        id: id
    }
}

export function addProductToList(product) {
    return {
        type: "ADD_TO_LIST",
        product: product
    }
}

export function onDataRequestSuccess(data) {
    return {
        type: 'RECEIVED_DATA',
        data: data
    }
}

export function onDataRequestError(error) {
    return {
        type: 'DATA_REQUEST_ERROR',
        error: error
    }
}