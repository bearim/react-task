import React from 'react';
import './static/style/App.css';
import {Routes} from "./routes/Routes";


const App = () =>
    (
        <div className="App">
            <Routes/>
        </div>
    );

export default App;
