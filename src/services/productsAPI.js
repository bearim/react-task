import axios from 'axios';

export const getAllProducts = () => {
    return axios.get('http://demo8936049.mockable.io/reactTask/getProducts');
}