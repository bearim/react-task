import React from 'react';
import QuantityInput from "../common/QuantityInput";
import {FaTrashAlt} from "react-icons/fa";
import {IoIosLink} from "react-icons/io";
import {ProductIcons as icons} from "../common/ProductIcons";
import CustomButton from "../common/CustomButton";
import {connect} from "react-redux";
import * as actions from '../../actions';

let ProductItem = ({productEntry: productItem, iconIndex, onProductInfoClick, id, dispatch}) => {

    const countTotal = () => {
        return productItem.price * productItem.quantity;
    };

    const onQuantityChange = newQuantity => {
        dispatch(actions.changeQuantity(id, newQuantity));
    };

    const onDelete = () => {
        dispatch(actions.deleteProductEntry(id));
    };

    const onOpenInfoClick = () => {
        onProductInfoClick({
            ...productItem,
            total: countTotal(),
            iconIndex: iconIndex
        });
    };

    return (
        <div className="ProductEntry">
            <label>{productItem.name}</label>

            <CustomButton buttonClass="ActionsIcon" buttonHandler={onOpenInfoClick} buttonContent={<IoIosLink size={20}/>}/>

            <CustomButton buttonClass="ActionsIcon" buttonHandler={onDelete} buttonContent={<FaTrashAlt size={20}/>}/>

            <div className="Icon">
                {icons[iconIndex].icon}
            </div>

            <QuantityInput quantity={productItem.quantity} onQuantityChange={onQuantityChange}/>

            <label>Total: {countTotal()} $</label>
        </div>
    );
};

export default connect()(ProductItem);
