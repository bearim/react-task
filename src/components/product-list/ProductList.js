import React, {useEffect, useState} from 'react';
import '../../static/style/App.css';
import ProductItem from "./ProductItem";
import {ProductIcons as icons} from "../common/ProductIcons";
import CustomButton from "../common/CustomButton";
import {connect} from "react-redux";
import * as actions from '../../actions';
import {getAllProducts} from "../../services/productsAPI";

let ProductList = ({dispatch, productList: productList}) => {

    useEffect(() => {
        getAllProducts().then(response => {
            dispatch(actions.onDataRequestSuccess(response.data))
        }).catch(error => {
                dispatch(actions.onDataRequestError(error));
            });
    }, []);

    const [showProductDetails, setShowProductDetails ] = useState();
    const [displayedProduct, setDisplayedProduct] = useState();

    const onShowProductDetailsClick = productEntry => {
        setDisplayedProduct(productEntry);
        setShowProductDetails(true);
    };

    const countTotalQuantity = () => {
        return productList.reduce((total, entry) => total + entry.price * entry.quantity, 0);
    };

    return (
        showProductDetails ? (
            <div className="Block">
                <span>{displayedProduct.name}</span>

                <div className="Icon" style={{float: 'none'}}>
                    {icons[displayedProduct.iconIndex].icon}
                </div>

                <div>
                    <label>Count: {displayedProduct.quantity}</label>
                </div>

                <div>
                    <label>Price: {displayedProduct.price} $</label>
                </div>

                <div>
                    <label>Total: {displayedProduct.total} $</label>
                </div>

                <CustomButton buttonClass="btn btn-primary DefaultButton" buttonContent="Back To List"
                              buttonHandler={() => setShowProductDetails(false)}/>
            </div>
        ) : (
            <div className="Block">
                <span>Product list</span>

                {productList.length === 0 ? (<div/>) : productList.map(product => (

                    <ProductItem productEntry={product} key={product.id} id={product.id} iconIndex={product.currentIcon}
                             onProductInfoClick={onShowProductDetailsClick}/>))

                }

                <label style={{float: 'left'}}>Total: {countTotalQuantity()} $</label>
            </div>
        )
    );
};

function mapStateToProps(state) {
    return {
        productList: state
    };
}

export default connect(mapStateToProps)(ProductList);



