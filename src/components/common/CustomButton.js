import React from 'react';
import '../../static/style/App.css';

const CustomButton = ({buttonClass, buttonHandler, isDisabled, buttonContent}) => (
    <button className={buttonClass} onClick={buttonHandler} disabled={isDisabled}>
        {buttonContent}
    </button>
);

export default CustomButton;
