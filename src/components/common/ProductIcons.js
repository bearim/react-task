import React from "react";
import {FaCarrot, FaApple, FaFish} from "react-icons/fa";
import {GoBook} from "react-icons/go";

export const ProductIcons = [
    {
        icon: <GoBook size={100}/>
    },
    {
        icon: <FaCarrot size={100}/>
    },
    {
        icon: <FaApple size={100}/>
    },
    {
        icon: <FaFish size={100}/>
    },

];
