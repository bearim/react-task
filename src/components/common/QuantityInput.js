import React from 'react';
import CustomButton from "./CustomButton";


const QuantityInput = ({quantity: quantity, onQuantityChange}) => {

    return (
        <div>
            <CustomButton buttonClass="btn btn-primary QuantityButton btn-group-lg"
                          buttonHandler={() => onQuantityChange(quantity - 1)} isDisabled={quantity === 1} buttonContent="-">
            </CustomButton>

            <label>{quantity}</label>

            <CustomButton buttonClass="btn btn-primary QuantityButton btn-group-lg"
                          buttonHandler={() => onQuantityChange(quantity + 1)} buttonContent="+">
            </CustomButton>
        </div>
    );
};

export default QuantityInput;
