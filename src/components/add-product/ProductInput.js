import React, {useState} from 'react';
import '../../static/style/App.css';
import QuantityInput from "../common/QuantityInput";
import ProductIconMenu from "./ProductIconMenu";
import CustomButton from "../common/CustomButton";
import uuid from 'react-uuid';
import {connect} from "react-redux";
import * as actions from '../../actions';

let ProductInput = ({dispatch}) => {
    const emptyProduct = {
        name: '',
        price: '',
        quantity: 1,
        currentIcon: 0
    };

    const [product, setProduct] = useState(emptyProduct);

    const isAddToListDisabled = () => {
        return product.name === '' || product.price === '' || product.price <= 0;
    };

    const productInputModified = field => e => setProduct({...product, [field]: e.target.value});

    const modifyProductField = (field, value) => {
        setProduct({...product, [field]: value});
    };

    const addToListHandler = () => {
        dispatch(actions.addProductToList(product));
        setProduct(emptyProduct);
    };

    const onQuantityChange = (newQuantity) => {
        modifyProductField('quantity', newQuantity);
    };

    const onIconSelected = (iconIndex) => {
        modifyProductField('currentIcon', iconIndex);
    };

    return (
        <div className="Block InputsBlock">
            <span>Add product to your cart list</span>

            <input id="productNameInputId" className="form-control" placeholder="Product name"
                   value={product.name} onChange={productInputModified('name')}/>

            <input id="productPriceInputId" className="form-control" placeholder="Product price"
                   type="number" value={product.price} onChange={productInputModified('price')}/>

            <QuantityInput key={uuid()} onQuantityChange={onQuantityChange} quantity={product.quantity}/>

            <ProductIconMenu currentIconIndex={product.currentIcon} onNewIconSelected={onIconSelected}/>

            <CustomButton buttonClass="btn btn-primary DefaultButton" buttonContent="Add to list"
                          isDisabled={isAddToListDisabled()} buttonHandler={addToListHandler}
            />
        </div>
    );
};

export default connect()(ProductInput);
