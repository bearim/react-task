import React, {useState} from 'react';
import {ProductIcons as icons} from '../common/ProductIcons'
import CustomButton from "../common/CustomButton";


const ProductIconMenu = ({currentIconIndex, onNewIconSelected}) => {

    const [showIconMenu, setShowIconMenu] = useState(false);

    const showIcon = (index) => {
        return icons[index].icon;
    };

    const onIconSelected = (index) => {
        setShowIconMenu(false);
        onNewIconSelected(index);
    };

    const toggleMenu = () => {
        setShowIconMenu(!showIconMenu);
    };

    return (
        <div>
            <CustomButton buttonHandler={toggleMenu} buttonClass="IconEntry"
                          buttonContent={showIcon(currentIconIndex)}/>

            {showIconMenu &&

            <div className="IconMenu">
                {
                    icons.length === 0 ? (<div/>) : icons.map((entry, index) => (

                    <CustomButton key={index} buttonClass="IconMenuEntry"
                                  buttonHandler={() => onIconSelected(index)}
                                  buttonContent={entry.icon}/>

                    ))
                }
            </div>
            }
        </div>
    )
};

export default ProductIconMenu;
